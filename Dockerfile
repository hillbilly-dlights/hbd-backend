FROM node:12-alpine
LABEL maintainer="DevOps <devops@wiedenba.ch>"

RUN mkdir /hbd-backend
COPY . /hbd-backend
WORKDIR /hbd-backend

EXPOSE 3000

CMD yarn start > operation.log
