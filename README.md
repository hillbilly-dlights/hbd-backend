# hbd-backend

Backend server for hillbilly-dlights.

## Development

### Project setup

To install the dependencies simply run `yarn`.

### Scripts

| Script | Description |
| --- | --- |
| `yarn dev` | Starts the development environment with live reload. |
| `yarn lint` | Runs eslint to lint the javascript files. |
| `yarn start` | Starts the express server. |
