const express = require('express');
const morgan = require('morgan');
const path = require('path');

const app = express();

const PUBLIC_DIRECTORY_PATH = path.join(__dirname, '..', 'public');
const PORT = 3000;

app.use(morgan('combined'));

app.use(express.static(PUBLIC_DIRECTORY_PATH));

app.get('*', (req, res) => {
    res.sendFile(path.join(PUBLIC_DIRECTORY_PATH, 'index.html'));
});

app.listen(PORT);

console.log(`Server started on port ${PORT}`);
