# hbd-backend Changelog

## [0.0.1] The first prototype

### Fixed

*nothing*

### Added

- Express server
- Gitlab CI
- Dockerfile and build process
- Readme
- License
- Changelog
- Git ignore
- Package.json
- Eslint

### Changed

*nothing*

### Removed

*nothing*
